package com.epam.tat.page.cloud;

import com.epam.tat.framework.bo.cloud.Folder;
import org.openqa.selenium.By;
import java.io.File;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class CloudPage {

    private static final By CREATE_BUTTON = By.xpath("//*[@id='toolbar-left']//div[@class='b-dropdown__ctrl']");
    private static final By FOLDER_BUTTON = By.xpath("//*[@id='toolbar-left']//a[@data-name='folder']");
    private static final By NAME_FOLDER = By.xpath("//*[@id='app']//form//input");
    private static final By ADD_BUTTON = By.xpath("//*[@id='app']//form//button[@data-name='add']");
    private static final By DELETE_BUTTON = By.xpath("//*[@id='toolbar']//div[@data-name='remove']");
    private static final By EXACT_DELETE = By.xpath("//*[@id='app']//button[@data-name='remove']");
    private static final By ACCEPT_BUTTON = By.xpath("//div[@class='layer_trashbin-tutorial']"
            + "//button[@data-name='close']");
    private static final By UPLOAD_BUTTON = By.xpath("//*[@id='toolbar-left']//div[@data-name='upload']");
    private static final By CHOOSE_FILE = By.xpath("//div[@class='layer_upload__controls']//input[@type='file']");
    private static final By MOVE_ACCEPT_BUTTON = By.xpath("//button[@data-name='move']");
    private static final By FILE_UPLOADED_MESSAGE = By.xpath("//span[text()='Загрузка завершена']");
    private static final String FOLDER_TITLE_PATTERN = "//span[text()='%s']";
    private static final String FILE_LOCATOR_PATTERN = "//div[contains(@data-id,'%s')]";
    private static final String FILE_CHECKBOX_LOCATOR_PATTERN = "//*[@id='datalist']//div[@data-id='/%s']"
            + "//div[@class='b-checkbox__box']";

    public CloudPage clickOnCreateButton() {
        getInstance().clickAndHighlightElement(CREATE_BUTTON);
        return this;
    }

    public CloudPage clickOnFolderButton() {
        getInstance().clickAndHighlightElement(FOLDER_BUTTON);
        return this;
    }

    public CloudPage typeFolderName(String folder) {
        getInstance().type(NAME_FOLDER, folder);
        return this;
    }

    public CloudPage clickOnAddButton() {
        getInstance().clickAndHighlightElement(ADD_BUTTON);
        return this;
    }

    public Boolean isFolderVisible(Folder folder) {
        return getInstance().isVisible(By.xpath(String.format(FOLDER_TITLE_PATTERN, folder.getName())));
    }

    public CloudPage clickOnCheckboxFolder(String folder) {
        getInstance().clickAndHighlightElement(By.xpath(String.format(FILE_CHECKBOX_LOCATOR_PATTERN, folder)));
        return this;
    }

    public CloudPage clickOnDelete() {
        getInstance().clickAndHighlightElement(DELETE_BUTTON);
        return this;
    }

    public CloudPage clickOnExactDelete() {
        getInstance().clickAndHighlightElement(EXACT_DELETE);
        getInstance().clickAndHighlightElement(ACCEPT_BUTTON);
        return this;
    }

    public CloudPage clickOnUploadFile() {
        getInstance().clickAndHighlightElement(UPLOAD_BUTTON);
        return this;
    }

    public CloudPage chooseFile(File file) {
        getInstance().getWrappedDriver().findElement(CHOOSE_FILE).sendKeys(file.getAbsolutePath());
        getInstance().waitForVisibilityOfElement(FILE_UPLOADED_MESSAGE);
        return this;
    }

    public Boolean isExactFile(String file) {
        return getInstance().isVisible(By.xpath(String.format(FILE_LOCATOR_PATTERN, file)));
    }

    public CloudPage dragAndDropExactFile(String file, String folder) {
        getInstance().refresh();
        By fileToDrag = By.xpath(String.format(FILE_LOCATOR_PATTERN, file));
        By folderToDrop = By.xpath(String.format(FILE_LOCATOR_PATTERN, folder));
        getInstance().dragAndDrop(fileToDrag, folderToDrop);
        getInstance().clickAndHighlightElement(MOVE_ACCEPT_BUTTON);
        getInstance().waitForInvisibilityOfElement(MOVE_ACCEPT_BUTTON);
        return this;
    }

    public CloudPage clickOnExactFolder(String folder) {
        getInstance().click(By.xpath(String.format(FILE_LOCATOR_PATTERN, folder)));
        return this;
    }
}
