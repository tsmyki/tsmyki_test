package com.epam.tat.page.mail;

import org.openqa.selenium.By;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class HomePage {

    private static final By USER_NAME_LABEL = By.xpath("//span/i[@id='PH_user-email']");
    private static final By COMPOSE_BUTTON = By.cssSelector("a[data-name ='compose']");
    private static final By INBOX_LINK = By.cssSelector("div[data-id='0']>a");
    private static final By SENT_LINK = By.cssSelector("a[href='/messages/sent/']");
    private static final By DRAFTS_LINK = By.cssSelector("a[href='/messages/drafts/']");
    private static final By TRASH_LINK = By.cssSelector("a[href='/messages/trash/']");
    private static final By CLOUD_LINK = By.xpath("//*[@id='portal-menu']//a[contains (@href,'cloud')]");
    private static final String EXACT_LETTER = "//div[text()='%s']";
    private static final String EXACT_LETTER_BY_SENDER = "//div[text()='%s' and contains"
            + "(@class, 'b-datalist__item__addr')]";
    private static final String EXACT_LETTER_IN_INBOX = "//div[@class='b-datalist__item__panel']//div[text()='%s']";

    public String getCurrentUserName() {
        return getInstance().getText(USER_NAME_LABEL);
    }

    public EmailPage clickOnComposeButton() {
        getInstance().click(COMPOSE_BUTTON);
        return new EmailPage();
    }

    public HomePage clickOnInboxLink() {
        getInstance().click(INBOX_LINK);
        return this;
    }

    public void clickOnSentLink() {
        getInstance().click(SENT_LINK);
    }

    public void clickOnDraftsLink() {
        getInstance().click(DRAFTS_LINK);
    }

    public void clickOnTrashLink() {
        getInstance().click(TRASH_LINK);
    }

    public Boolean isExactLetter(String topic) {
        return getInstance().isVisible(By.xpath(String.format(EXACT_LETTER, topic)));
    }

    public Boolean isExactLetterBySender(String sender) {
        return getInstance().isVisible(By.xpath(String.format(EXACT_LETTER_BY_SENDER, sender)));
    }

    public void clickOnCloudLink() {
        getInstance().click(CLOUD_LINK);
    }

    public EmailPage clickOnExactLetterInInbox(String topicCloudLink) {
        getInstance().click(By.xpath(String.format(EXACT_LETTER_IN_INBOX, topicCloudLink)));
        return new EmailPage();
    }
}
