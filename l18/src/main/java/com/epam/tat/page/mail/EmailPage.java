package com.epam.tat.page.mail;

import org.openqa.selenium.By;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class EmailPage {

    private static final By INPUT_FOR_ADDRESS = By.xpath("//textarea[@data-original-name='To']");
    private static final By INPUT_FOR_TOPIC = By.xpath("//input[@name='Subject']");
    private static final By TEXT_INPUT = By.xpath("//*[@id='tinymce']");
    private static final By SEND_BUTTON = By.xpath("//div[@data-name='send']");
    private static final By SAVE_BUTTON = By.xpath("//div[@data-name='saveDraft']");
    private static final By TEXT_FRAME = By.xpath("//iframe[@title='{#aria.rich_text_area}']");
    private static final By CLOUD_LINK = By.xpath("//div[@class='b-letter__body']//a[contains(@href,'cloud.mail.ru')]");
    private static final By CONTINUE_BUTTON = By.xpath("//div[@class='is-compose-empty_in']//button[@type='submit']");
    private static final By SAVE_MESSAGE = By.cssSelector("div[data-mnemo='saveStatus']");
    private static final By SEND_MESSAGE = By.xpath("//div[@class='message-sent__title']");

    public EmailPage fillInputForAddress(String email) {
        getInstance().type(INPUT_FOR_ADDRESS, email);
        return this;
    }

    public EmailPage fillInputForTopic(String topic) {
        getInstance().type(INPUT_FOR_TOPIC, topic);
        return this;
    }

    public EmailPage fillTextField(String text) {
        getInstance().switchToFrame(TEXT_FRAME);
        getInstance().type(TEXT_INPUT, text);
        getInstance().switchToFrame(null);
        return this;
    }

    public EmailPage clickOnSaveButton() {
        getInstance().click(SAVE_BUTTON);
        getInstance().waitForVisibilityOfElement(SAVE_MESSAGE);
        return this;
    }

    public EmailPage clickOnSendButton() {
        getInstance().click(SEND_BUTTON);
        return this;
    }

    public EmailPage clickOnContinueButton() {
        getInstance().click(CONTINUE_BUTTON);
        return this;
    }

    public boolean isContinueButton() {
        return getInstance().isVisible(CONTINUE_BUTTON);
    }

    public HomePage waitForSendMessage() {
        getInstance().waitForVisibilityOfElement(SEND_MESSAGE);
        return new HomePage();
    }

    public void clickOnLinkInTextField() {
        getInstance().click(CLOUD_LINK);
    }
}
