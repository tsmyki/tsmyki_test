package com.epam.tat.page.mail;

import org.openqa.selenium.By;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class LoginPage {

    private static final String URL = "https://mail.ru";
    private static final By LOGIN_INPUT = By.xpath("//input[@id='mailbox__login']");
    private static final By PASS_INPUT = By.xpath("//input[@id='mailbox__password']");
    private static final By SIGN_IN_BUTTON = By.xpath("//input [@id='mailbox__auth__button']");
    private static final By ERROR_MESSAGE = By.xpath("//div[@id='mailbox:authfail']");

    public LoginPage open() {
        getInstance().get(URL);
        return this;
    }

    public LoginPage setLogin(String login) {
        getInstance().type(LOGIN_INPUT, login);
        return this;
    }

    public LoginPage setPassword(String password) {
        getInstance().type(PASS_INPUT, password);
        return this;
    }

    public void signIn() {
        getInstance().click(SIGN_IN_BUTTON);
    }

    public boolean hasErrorMessage() {
        return getInstance().isVisible(ERROR_MESSAGE);
    }

    public String getErrorMessage() {
        return getInstance().getText(ERROR_MESSAGE);
    }
}
