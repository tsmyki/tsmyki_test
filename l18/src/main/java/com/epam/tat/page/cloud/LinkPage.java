package com.epam.tat.page.cloud;

import org.openqa.selenium.By;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class LinkPage {

    private static final By GET_LINK = By.xpath("//a[@data-name='publish']/span");
    private static final By SEND_LINK_BUTTON = By.xpath("//button[@data-id='email']");
    private static final String EXACT_FOLDER = "//*[@id='datalist']//span[text()='%s']";

    public LinkPage clickOnShareButton(String folder) {
        getInstance().contextClick(By.xpath(String.format(EXACT_FOLDER, folder)));
        return this;
    }

    public LinkPage getLinkToShare() {
        getInstance().clickAndHighlightElement(GET_LINK);
        return this;
    }

    public void clickOnSendLinkButton() {
        getInstance().clickAndHighlightElement(SEND_LINK_BUTTON);
    }
}
