package com.epam.tat.page.mail;

import org.openqa.selenium.By;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class LetterListPage {

    private static final By DELETE_BUTTON = By.xpath("//div[@data-cache-key and (@style='' or not(@style))]"
            + "//div[@data-name='remove']");
    private static final String EXACT_CHECKBOX_PATTERN = "//a[@data-subject='%s']"
            + "//div[@class='js-item-checkbox b-datalist__item__cbx']";
    private static final By NOTIFICATION = By.cssSelector(".notify-message__title__text."
            + "notify-message__title__text_ok");

    public LetterListPage selectLetterByTopic(String topic) {
        getInstance().click(By.xpath(String.format(EXACT_CHECKBOX_PATTERN, topic)));
        return this;
    }

    public void clickOnDelete() {
        getInstance().click(DELETE_BUTTON);
        getInstance().waitForVisibilityOfElement(NOTIFICATION);
    }
}
