package com.epam.tat.test.common;

import com.epam.tat.framework.bo.common.User;
import com.epam.tat.framework.bo.common.UserFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends Configuration {

    @Test(description = "Test verifies that user can login with correct credentials")
    public void loginWithCorrectPassword() {
        authenticationService.login(validUser);
        Assert.assertTrue(authenticationService.isUserLoggedIn(validUser),
                "Invalid result, user can't login with correct credentials");
    }

    @Test(description = "Test verifies that user gets error message, when he logins with incorrect password",
            expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = errorMessage1)
    public void loginWithInCorrectPassword() {
        User user = UserFactory.generateUser(email, incorrectPass);
        authenticationService.login(user);
    }

    @Test(description = "Test verifies that user gets error message, when he logins without login",
            expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = errorMessage2)
    public void loginWithoutLogin() {
        User user = UserFactory.generateUser("", correctPass);
        authenticationService.login(user);
    }

    @Test(description = "Test verifies that user gets error message, when he logins without password",
            expectedExceptions = RuntimeException.class, expectedExceptionsMessageRegExp = errorMessage3)
    public void loginWithoutPassword() {
        User user = UserFactory.generateUser(email, "");
        authenticationService.login(user);
    }
}
