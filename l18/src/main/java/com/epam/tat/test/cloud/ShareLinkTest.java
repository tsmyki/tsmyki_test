package com.epam.tat.test.cloud;

import com.epam.tat.framework.bo.cloud.Folder;
import com.epam.tat.framework.bo.cloud.FolderFactory;
import com.epam.tat.framework.bo.mail.MailFolder;
import com.epam.tat.test.common.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ShareLinkTest extends Configuration {

    @BeforeMethod(description = "Login to mail and go to Cloud")
    public void loginAndGoToCloud() {
        authenticationService.loginAndGoToCloud(validUser);
    }

    @Test(description = "Сreate folder in cloud and share link to this folder")
    public void shareLinkToExactFolder() {
        Folder folder = FolderFactory.generateDefaultFolder();
        cloudService.createFolder(folder);
        cloudService.shareLinkByEmail(folder, email);
        letterService.navigateToFolder(MailFolder.INBOX);
        letterService.openLetterInFolder(MailFolder.INBOX, topicCloudLink);
        letterService.openLinkInEmail();
        Assert.assertTrue(cloudService.isFolderPresentInCloud(folder),
                "Link to folder that user sent not appeared in mail");
    }
}
