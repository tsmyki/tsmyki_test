package com.epam.tat.test.mail;

import com.epam.tat.framework.bo.mail.Letter;
import com.epam.tat.framework.bo.mail.LetterFactory;
import com.epam.tat.framework.bo.mail.MailFolder;
import com.epam.tat.test.common.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SendEmailTest extends Configuration {

    @BeforeMethod(description = "Login with correct credentials")
    public void login() {
        authenticationService.login(validUser);
    }

    @Test(description = "Test verifies that user can send email with all fields and find exact email in inbox")
    public void sendEmailAndCheckInbox() {
        Letter letter = LetterFactory.generateDefaultLetter();
        letterService.sendEmail(letter);
        Assert.assertTrue(letterService.isLetterPresentInFolder(MailFolder.INBOX, letter),
                "Letter that user sent not appeared in Inbox folder");
    }

    @Test(description = "Test verifies that user can send email with all fields and find exact email in sent")
    public void sendEmailAndCheckSent() {
        Letter letter = LetterFactory.generateDefaultLetter();
        letterService.sendEmail(letter);
        Assert.assertTrue(letterService.isLetterPresentInFolder(MailFolder.SENT, letter),
                "Letter that user sent not appeared in Sent folder");
    }
}
