package com.epam.tat.test.cloud;

import com.epam.tat.framework.bo.cloud.Folder;
import com.epam.tat.framework.bo.cloud.FolderFactory;
import com.epam.tat.test.common.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreateAndDeleteFolderTest extends Configuration {

    @BeforeMethod(description = "Login to mail and go to Cloud")
    public void loginAndGoToCloud() {
        authenticationService.loginAndGoToCloud(validUser);
    }

    @Test(description = "Test verifies that user can create folder in cloud")
    public void createAndCheckFolder() {
        Folder folder = FolderFactory.generateDefaultFolder();
        cloudService.createFolder(folder);
        Assert.assertTrue(cloudService.isFolderPresentInCloud(folder),
                "Folder that user created not appeared in cloud");
    }

    @Test(description = "Test verifies that user can create and delete folder in cloud ")
    public void deleteAndCheckFolderNotFound() {
        Folder folder = FolderFactory.generateDefaultFolder();
        cloudService.createFolder(folder);
        cloudService.deleteFolder(folder);
        Assert.assertFalse(cloudService.isFolderPresentInCloud(folder),
                "Folder that user deleted doesn't have to be found in cloud");
    }
}
