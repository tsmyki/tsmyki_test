package com.epam.tat.test.mail;

import com.epam.tat.framework.bo.mail.Letter;
import com.epam.tat.framework.bo.mail.LetterFactory;
import com.epam.tat.test.common.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SendEmailWithWrongAddressTest extends Configuration {

    @BeforeMethod(description = "Login with correct credentials")
    public void login() {
        authenticationService.login(validUser);
    }

    @Test(description = "Test verifies, if user fills wrong address,"
            + "he can't send email and gets correct error message", expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = alertMessage)
    public void sendEmailWithWrongAddress() {
        Letter letter = LetterFactory.generateLetter(wrongEmail);
        letterService.sendEmail(letter);
    }
}
