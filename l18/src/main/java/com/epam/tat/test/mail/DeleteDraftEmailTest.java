package com.epam.tat.test.mail;

import com.epam.tat.framework.bo.mail.Letter;
import com.epam.tat.framework.bo.mail.LetterFactory;
import com.epam.tat.framework.bo.mail.MailFolder;
import com.epam.tat.test.common.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DeleteDraftEmailTest extends Configuration {

    @BeforeMethod(description = "Login with correct credentials")
    public void login() {
        authenticationService.login(validUser);
    }

    @Test(description = "Test verifies that user can save email,"
            + " delete it in drafts and find exact email in trash")
    public void checkDeletedDraftEmail() {
        Letter letter = LetterFactory.generateDefaultLetter();
        letterService.createDraft(letter);
        letterService.deleteLetter(MailFolder.DRAFTS, letter);
        Assert.assertTrue(letterService.isLetterPresentInFolder(MailFolder.TRASH, letter),
                "Letter that user deleted not appeared in trash");
    }

    @Test(description = "Test verifies that user can save email, delete it in drafts and in trash")
    public void deleteDraftEmailPermanently() {
        Letter letter = LetterFactory.generateDefaultLetter();
        letterService.createDraft(letter);
        letterService.deleteLetter(MailFolder.DRAFTS, letter);
        letterService.deleteLetter(MailFolder.TRASH, letter);
        Assert.assertFalse(letterService.isLetterPresentInFolder(MailFolder.TRASH, letter),
                "Letter that user deleted in trash doesn't have to be found");
    }
}
