package com.epam.tat.test.common;

import com.epam.tat.framework.bo.common.User;
import com.epam.tat.framework.bo.common.UserFactory;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.service.cloud.CloudService;
import com.epam.tat.service.common.AuthenticationService;
import com.epam.tat.service.mail.LetterService;
import org.testng.annotations.AfterMethod;

public class Configuration {

    protected final String sender = "Tanya Sm";
    protected final String email = "tsmyki10@mail.ru";
    protected final String wrongEmail = "111111123";
    protected final String correctPass = "hh55dvnm0";
    protected final String incorrectPass = "123";
    protected final String topicCloudLink = "Папка из Облака Mail.Ru";
    protected final String errorMessage1 = "Неверное имя или пароль";
    protected final String errorMessage2 = "Введите имя ящика";
    protected final String errorMessage3 = "Введите пароль";
    protected final String alertMessage = "The \"To\" field has an invalid recipient email address.\n"
            + "Correct the error and try sending again.";
    protected AuthenticationService authenticationService = new AuthenticationService();
    protected LetterService letterService = new LetterService();
    protected CloudService cloudService = new CloudService();
    protected final User validUser = UserFactory.generateDefaultUser();

    @AfterMethod
    public void quitDriver() {
        Browser.getInstance().stopBrowser();
    }
}
