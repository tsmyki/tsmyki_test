package com.epam.tat.test.cloud;

import com.epam.tat.framework.bo.cloud.Folder;
import com.epam.tat.framework.bo.cloud.FolderFactory;
import com.epam.tat.framework.utils.RandomUtils;
import com.epam.tat.test.common.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;

public class UploadFileTest extends Configuration {
    private File file;

    @BeforeMethod(description = "Login to mail and go to Cloud")
    public void loginAndGoToCloud() {
        authenticationService.loginAndGoToCloud(validUser);
    }

    @BeforeMethod(description = "Upload file to cloud", dependsOnMethods = "loginAndGoToCloud")
    public void uploadFile() {
        file = RandomUtils.createRandomFile();
        cloudService.uploadFile(file);
    }

    @Test(description = "Test verifies that user can upload file to cloud")
    public void checkUploadedFile() {
        Assert.assertTrue(cloudService.isFilePresentInCloud(file),
                "File that user upload not appeared in cloud");
    }

    @Test(description = "Test verifies that user can move file to exact folder")
    public void moveFileToFolder() {
        Folder folder = FolderFactory.generateDefaultFolder();
        cloudService.createFolder(folder);
        cloudService.moveFileToFolder(file, folder);
        cloudService.openFolder(folder);
        Assert.assertTrue(cloudService.isFilePresentInCloud(file),
                "File that user move to folder not appeared in this folder");
    }
}
