package com.epam.tat.service.cloud;

import com.epam.tat.framework.bo.cloud.Folder;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.page.cloud.CloudPage;
import com.epam.tat.page.cloud.LinkPage;
import com.epam.tat.page.mail.EmailPage;
import java.io.File;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class CloudService {

    public void createFolder(Folder folder) {
        Log.debug(String.format("Create Folder: \"%s\"", folder.getName()));
        new CloudPage()
                .clickOnCreateButton()
                .clickOnFolderButton()
                .typeFolderName(folder.getName())
                .clickOnAddButton();
    }

    public void deleteFolder(Folder folder) {
        Log.debug(String.format("Delete Folder: \"%s\"", folder.getName()));
        new CloudPage()
                .clickOnCheckboxFolder(folder.getName())
                .clickOnDelete()
                .clickOnExactDelete();
    }

    public boolean isFolderPresentInCloud(Folder folder) {
        Log.debug(String.format("Check Folder: \"%s\" in Cloud", folder.getName()));
        return new CloudPage().isFolderVisible(folder);
    }

    public boolean isFilePresentInCloud(File file) {
        Log.debug(String.format("Check File: \"%s\" in Cloud", file));
        return new CloudPage().isExactFile(file.getName());
    }

    public void uploadFile(File file) {
        Log.debug(String.format("Upload File: \"%s\" to Cloud", file));
        new CloudPage()
                .clickOnUploadFile()
                .chooseFile(file);
    }

    public void moveFileToFolder(File file, Folder folder) {
        Log.debug(String.format("Move File: \"%s\" to Folder: \"%s\"", file, folder.getName()));
        new CloudPage()
                .dragAndDropExactFile(file.getName(), folder.getName());
    }

    public void openFolder(Folder folder) {
        Log.debug(String.format("Open Folder: \"%s\"", folder.getName()));
        new CloudPage()
                .clickOnExactFolder(folder.getName());
    }

    public void shareLinkByEmail(Folder folder, String email) {
        Log.debug(String.format("Share link to Folder: \"%s\" to: %s", folder.getName(), email));
        new LinkPage()
                .clickOnShareButton(folder.getName())
                .getLinkToShare()
                .clickOnSendLinkButton();
        getInstance().switchToNewWindow();
        new EmailPage()
                .fillInputForAddress(email)
                .clickOnSendButton()
                .waitForSendMessage();
    }
}
