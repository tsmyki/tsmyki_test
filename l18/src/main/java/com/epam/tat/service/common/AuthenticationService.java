package com.epam.tat.service.common;

import com.epam.tat.framework.bo.common.User;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.page.mail.HomePage;
import com.epam.tat.page.mail.LoginPage;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class AuthenticationService {

    public void login(User user) {
        Log.debug(String.format("Login with credentials - login: %s, password: %s",
                user.getEmail(), user.getPassword()));
        LoginPage loginPage = new LoginPage();
        loginPage.open()
                .setLogin(user.getEmail())
                .setPassword(user.getPassword())
                .signIn();
        if (loginPage.hasErrorMessage()) {
            Log.debug(String.format("Login with credentials - login: %s, password: %s",
                    user.getEmail(), user.getPassword()));
            throw new RuntimeException(loginPage.getErrorMessage());
        }

        if (!isUserLoggedIn(user)) {
            Log.debug(String.format("Login with credentials - login: %s, password: %s",
                    user.getEmail(), user.getPassword()));
            throw new RuntimeException("User cannot login to mail");
        }
    }

    public boolean isUserLoggedIn(User user) {
        return new HomePage().getCurrentUserName().equals(user.getEmail());
    }

    public void loginAndGoToCloud(User user) {
        login(user);
        Log.debug("Go to Cloud");
        new HomePage().clickOnCloudLink();
        getInstance().switchToNewWindow();
    }
}
