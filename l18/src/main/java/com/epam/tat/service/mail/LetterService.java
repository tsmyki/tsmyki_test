package com.epam.tat.service.mail;

import com.epam.tat.framework.bo.mail.Letter;
import com.epam.tat.framework.bo.mail.MailFolder;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.page.mail.EmailPage;
import com.epam.tat.page.mail.HomePage;
import com.epam.tat.page.mail.LetterListPage;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class LetterService {

    public void sendEmail(Letter letter) {
        Log.debug(String.format("Send Email to: %s, with Topic: \"%s\"", letter.getSendTo(), letter.getTopic()));
        new HomePage()
                .clickOnComposeButton()
                .fillInputForAddress(letter.getSendTo())
                .fillInputForTopic(letter.getTopic())
                .fillTextField(letter.getBody())
                .clickOnSendButton();

        if (getInstance().isAlertPresent()) {
            String alertText = getInstance().getAlertText();
            throw new RuntimeException(alertText);
        } else if (new EmailPage().isContinueButton()) {
            new EmailPage().clickOnContinueButton()
                           .waitForSendMessage();
        } else {
            new EmailPage().waitForSendMessage();
        }
    }

    public void navigateToFolder(MailFolder folder) {
        Log.debug("Navigate to folder: " + folder);
        switch (folder) {
            case INBOX:
                new HomePage().clickOnInboxLink();
                break;
            case SENT:
                new HomePage().clickOnSentLink();
                break;
            case DRAFTS:
                new HomePage().clickOnDraftsLink();
                break;
            case TRASH:
                new HomePage().clickOnTrashLink();
                break;
            default:
                throw new RuntimeException("Not supported folder " + folder);
        }
    }

    public boolean isLetterPresentInFolder(MailFolder folder, Letter letter) {
        Log.debug(String.format("Check letter with Topic: \"%s\" in Folder: %s", letter.getTopic(), folder));
        navigateToFolder(folder);
        return new HomePage().isExactLetter(letter.getTopic());
    }

    public void openLetterInFolder(MailFolder folder, String topicCloudLink) {
        Log.debug(String.format("Open letter with Topic: \"%s\" in Folder: %s", topicCloudLink, folder));
        navigateToFolder(folder);
        new HomePage().clickOnExactLetterInInbox(topicCloudLink);
    }

    public void openLinkInEmail() {
        new EmailPage().clickOnLinkInTextField();
        getInstance().switchToNewWindow();
    }

    public boolean isLetterPresentInFolderBySender(MailFolder folder, String sender) {
        Log.debug(String.format("Check letter with Sender: \"%s\" in Folder: %s", sender, folder));
        navigateToFolder(folder);
        return new HomePage().isExactLetterBySender(sender);
    }

    public void createDraft(Letter letter) {
        Log.debug(String.format("Create draft with Addressee: %s and Topic: \"%s\"",
                letter.getSendTo(), letter.getTopic()));
        new HomePage()
                .clickOnComposeButton()
                .fillInputForAddress(letter.getSendTo())
                .fillInputForTopic(letter.getTopic())
                .fillTextField(letter.getBody())
                .clickOnSaveButton();
    }

    public void deleteLetter(MailFolder folder, Letter letter) {
        Log.debug(String.format("Delete letter with Topic: \"%s\" in Folder: %s", letter.getTopic(), folder));
        navigateToFolder(folder);
        new LetterListPage()
                .selectLetterByTopic(letter.getTopic())
                .clickOnDelete();
    }
}
