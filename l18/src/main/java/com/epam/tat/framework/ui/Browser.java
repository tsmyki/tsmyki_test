package com.epam.tat.framework.ui;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.runner.Parameters;
import com.thoughtworks.selenium.SeleniumException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class Browser implements WrapsDriver {

    private static final int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 10;

    private static ThreadLocal<Browser> instance = new ThreadLocal<Browser>();

    private WebDriver wrappedWebDriver;

    private Browser() {
        if (Parameters.instance().getBrowserType().equals(BrowserType.CHROME)) {
            System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
            wrappedWebDriver = new ChromeDriver();
        } else if (Parameters.instance().getBrowserType().equals(BrowserType.FIREFOX)) {
            System.setProperty("webdriver.gecko.driver", Parameters.instance().getGeckoDriver());
            wrappedWebDriver = new FirefoxDriver();
        }
        wrappedWebDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wrappedWebDriver.manage().window().maximize();
    }

    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void stopBrowser() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } catch (SeleniumException e) {
            e.printStackTrace();
        } finally {
            instance.set(null);
        }
    }

    public void get(String url) {
        Log.debug("WebDriver navigated to url: " + url);
        wrappedWebDriver.get(url);
        getScreenshot();
    }

    public void contextClick(By by) {
        Log.debug("ContextClick " + by);
        waitForVisibilityOfElement(by);
        Actions actions = new Actions(wrappedWebDriver);
        actions.contextClick(wrappedWebDriver.findElement(by)).perform();
    }

    public void click(By by) {
        Log.debug("Click " + by);
        waitForVisibilityOfElement(by);
        wrappedWebDriver
                .findElement(by)
                .click();
    }

    public void refresh() {
        wrappedWebDriver.navigate().refresh();
    }

    public void type(By by, String keys) {
        Log.debug("Type " + by);
        waitForVisibilityOfElement(by);
        wrappedWebDriver
                .findElement(by)
                .sendKeys(keys);
    }

    public String getText(By by) {
        Log.debug("GetText " + by);
        waitForVisibilityOfElement(by);
        return wrappedWebDriver.findElement(by).getText();
    }

    public void dragAndDrop(By from, By to) {
        Log.debug("DragFrom " + from);
        Log.debug("DropTo " + to);
        waitForVisibilityOfElement(from);
        waitForVisibilityOfElement(to);
        Actions actions = new Actions(wrappedWebDriver);
        actions.dragAndDrop(wrappedWebDriver.findElement(from), wrappedWebDriver.findElement(to)).perform();
    }

    public void switchToFrame(By by) {
        if (by == null) {
            wrappedWebDriver.switchTo().defaultContent();
        } else {
            Log.debug("SwitchToFrame " + by);
            wrappedWebDriver.switchTo().frame(wrappedWebDriver.findElement(by));
        }
    }

    public boolean isAlertPresent() {
        try {
            wrappedWebDriver.switchTo().alert();
        } catch (NoAlertPresentException e) {
            return false;
        }
        return true;
    }

    public String getAlertText() {
        return wrappedWebDriver.switchTo().alert().getText();
    }

    public boolean isVisible(By by) {
        try {
            waitForVisibilityOfElement(by);
        } catch (WebDriverException e) {
            return false;
        }
        return true;
    }

    public void switchToNewWindow() {
        String currentWindow = wrappedWebDriver.getWindowHandle();
        for (String windowHandle : wrappedWebDriver.getWindowHandles()) {
            if (!windowHandle.equals(currentWindow)) {
                wrappedWebDriver.switchTo().window(windowHandle);
                getScreenshot();
            }
        }
    }

    public void waitForVisibilityOfElement(By by) {
        Log.debug("WaitForVisibilityOfElement " + by);
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForInvisibilityOfElement(By by) {
        Log.debug("WaitForInVisibilityOfElement " + by);
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void clickAndHighlightElement(By by) {
        Log.debug("ClickAndHighlightElement " + by);
        ((JavascriptExecutor) wrappedWebDriver).executeScript("arguments[0].setAttribute('style', arguments[1]);",
                new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECONDS).
                        until(ExpectedConditions.visibilityOfElementLocated(by)),
                "color: DeepPink; border: double deeppink;");
        wrappedWebDriver
                .findElement(by)
                .click();
    }

    public void getScreenshot() {
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + "screenshot.png");
        try {
            File screenshotAsFile = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotAsFile, screenshotFile);
            Log.info("Screenshot taken: file: " + screenshotFile.getAbsolutePath());
            Reporter.log("<a href=\"../../" + screenshotFile + "\" target=\"_blank\">" + screenshotFile + "</a>");
        } catch (IOException e) {
            throw new RuntimeException("Failed screenshot: ", e);
        }
    }
}
