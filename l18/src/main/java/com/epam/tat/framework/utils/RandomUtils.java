package com.epam.tat.framework.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

public class RandomUtils {
    private static final int COUNT = 5;
    private static final int COUNT_LETTERS_IN_FILE = 100;

    public static File createRandomFile() {
        File file = FileUtils.getFile(String.format("RandomFile_%s.txt", RandomStringUtils.randomAlphanumeric(COUNT)));
        try {
            FileUtils.writeStringToFile(file, RandomStringUtils.randomAlphabetic(COUNT_LETTERS_IN_FILE),
                    Charset.defaultCharset());
        } catch (IOException e) {
            throw new RuntimeException("RandomFile cannot be created", e);
        }
        return file;
    }
}
