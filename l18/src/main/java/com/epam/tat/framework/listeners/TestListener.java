package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import static com.epam.tat.framework.ui.Browser.getInstance;

public class TestListener implements IInvokedMethodListener {

    private static final int SUCCESS_STATUS = 1;
    private static final int FAILED_STATUS = 2;
    private static final int SKIPPED_STATUS = 3;

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        Log.info("[TEST STARTED] " + iInvokedMethod.getTestMethod().getMethodName());
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        switch (iTestResult.getStatus()) {
            case SUCCESS_STATUS:
                Log.info("[TEST SUCCESS] " + iInvokedMethod.getTestMethod().getMethodName());
                break;
            case FAILED_STATUS:
                Log.error("[FAILED TEST]: " + iInvokedMethod.getTestMethod().getMethodName(),
                        iTestResult.getThrowable());
                getInstance().getScreenshot();
                break;
            case SKIPPED_STATUS:
                Log.info("[TEST SKIPPED] " + iInvokedMethod.getTestMethod().getMethodName());
                break;
            default:
                throw new RuntimeException("Not supported Status " + iTestResult.getStatus());
        }
    }
}
