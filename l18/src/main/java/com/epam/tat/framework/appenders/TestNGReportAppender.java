package com.epam.tat.framework.appenders;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

public class TestNGReportAppender extends AppenderSkeleton {
    @Override
    protected void append(LoggingEvent loggingEvent) {
        Reporter.log(layout.format(loggingEvent));
    }

    public void close() {
    }

    public boolean requiresLayout() {
        return true;
    }
}
