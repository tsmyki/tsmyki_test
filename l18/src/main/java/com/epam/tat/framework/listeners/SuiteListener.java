package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {

    public void onStart(ISuite iSuite) {
        Log.info("[SUITE STARTED] " + iSuite.getName());
    }

    public void onFinish(ISuite iSuite) {
        Log.info("[SUITE FINISHED] " + iSuite.getName());
    }
}
