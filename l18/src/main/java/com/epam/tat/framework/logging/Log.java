package com.epam.tat.framework.logging;

import org.apache.log4j.Logger;

public class Log {

    private static final Logger LOG = Logger.getLogger("com.epam.tat");

    public static void debug(Object message) {
        LOG.debug(message);
    }

    public static void info(Object message) {
        LOG.info(message);
    }

    public static void error(Object message, Throwable throwable) {
        LOG.error(message, throwable);
    }
}
