package com.epam.tat.framework.bo.mail;

import org.apache.commons.lang3.RandomStringUtils;

public class LetterFactory {

    private static final int COUNT = 5;
    private static final String DEFAULT_EMAIL = "tsmyki10@mail.ru";

    public static Letter generateLetterWithoutSubject(String email, String topic, String body) {
        Letter letter = new Letter();
        letter.setSendTo(email);
        letter.setTopic(topic);
        letter.setBody(body);
        return letter;
    }

    public static Letter generateLetter(String email) {
        Letter letter = new Letter();
        letter.setSendTo(email);
        letter.setTopic("Email Topic" + RandomStringUtils.randomAlphanumeric(COUNT));
        letter.setBody("Hello" + RandomStringUtils.randomAlphanumeric(COUNT));
        return letter;
    }

    public static Letter generateDefaultLetter() {
        Letter letter = new Letter();
        letter.setSendTo(DEFAULT_EMAIL);
        letter.setTopic("Email Topic" + RandomStringUtils.randomAlphanumeric(COUNT));
        letter.setBody("Hello" + RandomStringUtils.randomAlphanumeric(COUNT));
        return letter;
    }
}
