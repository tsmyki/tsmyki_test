package com.epam.tat.framework.bo.common;

public class UserFactory {

    private static final String DEFAULT_EMAIL = "tsmyki10@mail.ru";
    private static final String DEFAULT_PASSWORD = "hh55dvnm0";

    public static User generateUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        return user;
    }

    public static User generateDefaultUser() {
        User user = new User();
        user.setEmail(DEFAULT_EMAIL);
        user.setPassword(DEFAULT_PASSWORD);
        return user;
    }
}
