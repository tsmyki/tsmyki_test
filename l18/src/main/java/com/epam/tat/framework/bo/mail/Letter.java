package com.epam.tat.framework.bo.mail;

public class Letter {

    private String sendTo;
    private String topic;
    private String body;

    public String getSendTo() {
        return sendTo;
    }

    public String getTopic() {
        return topic;
    }

    public String getBody() {
        return body;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
