package com.epam.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.tat.framework.ui.BrowserType;
import java.util.ArrayList;
import java.util.List;

public final class Parameters {

    private static Parameters instance;

    @Parameter(names = "--help", help = true, description = "Help")
    private boolean help;

    @Parameter(names = {"--browser", "-b"}, description = "Browser type",
            converter = BrowserTypeConverter.class)
    private BrowserType browserType = BrowserType.FIREFOX;

    @Parameter(names = {"--chrome", "-c"}, description = "Path to ChromeDriver")
    private String chromeDriver = "src\\main\\resources\\chromedriver.exe";

    @Parameter(names = {"--gecko", "-g"}, description = "Path to GeckoDriver")
    private String geckoDriver = "src\\main\\resources\\geckodriver.exe";

    @Parameter(names = {"--suites", "-s"}, description = "Suites to launch")
    private List<String> suites = new ArrayList<String>() {{
            add("./src/test/resources/testngMail.xml");
            add("./src/test/resources/testngCloud.xml");
        }};

    @Parameter(names = {"--log4j", "-l"}, description = "Path to log4j")
    private String log4j = "log4j.properties";

    private Parameters() {

    }

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public List<String> getSuites() {
        return suites;
    }

    public String getLog4j() {
        return log4j;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public String getGeckoDriver() {
        return geckoDriver;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}
