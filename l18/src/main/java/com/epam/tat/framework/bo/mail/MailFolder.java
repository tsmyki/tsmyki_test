package com.epam.tat.framework.bo.mail;

public enum MailFolder {
    INBOX, SENT, DRAFTS, TRASH
}
