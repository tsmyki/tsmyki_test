package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.listeners.SuiteListener;
import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.logging.Log;
import org.apache.log4j.PropertyConfigurator;
import org.testng.TestNG;

public class TestRunner {

    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new SuiteListener());
        testNG.addListener(new TestListener());
        testNG.setTestSuites(Parameters.instance().getSuites());
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse cli");
        parseCli(args);
        Log.info("Start app");
        createTestNG().run();
        Log.info("Finish app");
    }

    private static void parseCli(String[] args) {
        Log.info("Parse clis using JCommander");
        JCommander jCommander = new JCommander(Parameters.instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage(), e);
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
        PropertyConfigurator.configure(Parameters.instance().getLog4j());
    }
}

