package com.epam.tat.framework.bo.cloud;

import org.apache.commons.lang3.RandomStringUtils;

public class FolderFactory {

    private static final int COUNT = 5;

    public static Folder generateDefaultFolder() {
        Folder folder = new Folder();
        folder.setName("Folder" + RandomStringUtils.randomAlphanumeric(COUNT));
        return folder;
    }
}
