package com.epam.tat.framework.bo.cloud;

public class Folder {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
